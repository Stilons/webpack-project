// svgLoader.ts
/// <reference types="webpack-env" />

// Подключаем SVG спрайты
function requireAll(r: __WebpackModuleApi.RequireContext): void {
    r.keys().forEach(r);
}

requireAll(require.context('./../assets/img/icons-svg', true, /\.svg$/));

// Загружаем спрайт SVG и вставляем его в элемент с id "svg-icons"
fetch(`/assets/img/sprite.svg?${Date.now()}`)
    .then(res => res.text())
    .then(data => {
        const svgIconsElement = document.getElementById('svg-icons');
        if (svgIconsElement) {
            svgIconsElement.innerHTML = data;
        } else {
            console.error('Element with id "svg-icons" not found.');
        }
    })
    .catch(error => {
        console.error('Error fetching SVG sprite:', error);
    });
