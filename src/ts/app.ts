import './../sass/main.scss';
import "./scripts";

import "./components/MenuController";

import {Modal, ModalOptions} from "./utils/Modalized";

function createModals(optionsArray: ModalOptions[]): void {
    optionsArray.forEach(options => new Modal(options));
}

const modalOptionsArray: ModalOptions[] = [
    {
        name: 'mobile-menu',
        headerVisible: true,
    },
]

document.addEventListener('DOMContentLoaded', () => {
    createModals(modalOptionsArray);
});