class MenuController {
    private readonly btn: HTMLElement | null = null;

    constructor() {
        this.btn = document.querySelector('.js-mobile-btn');
        if(this.btn) this.btn.addEventListener('click', this.init.bind(this))
    }

    private init() {
        this.btn!.classList.toggle('_active');
    }
}

document.addEventListener('DOMContentLoaded', function() {
    new MenuController()
})