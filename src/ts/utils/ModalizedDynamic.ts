export interface ModalizedDynamicOptions {
    modal: HTMLElement;
    config: { [key: string]: string };
}

export class ModalizedDynamic {
    /**
     * @type {HTMLElement}
     * @private
     */
    private modal: HTMLElement;

    /**
     * @type {Object.<string, string>}
     * @readonly
     * @private
     */
    private readonly config: { [key: string]: string };

    /**
     * @type {string}
     * @readonly
     * @private
     */
    private readonly param: string = 'data-dynamic-modal-field';

    /**
     * Создает экземпляр ModalizedDynamic.
     * @param {ModalizedDynamicOptions} options - Параметры для инициализации.
     */
    constructor(options: ModalizedDynamicOptions) {
        this.modal = options.modal;
        this.config = options.config;
        this.init();
    }

    /**
     * Инициализирует модальное окно, очищая старые значения и применяя новые из конфигурации.
     * @private
     */
    private init() {
        this.clearFields();
        this.applyConfig();
    }

    /**
     * Очищает все поля с атрибутом `data-dynamic-modal-field` внутри модального окна.
     * @private
     */
    private clearFields() {
        const elements = this.modal.querySelectorAll(`[${this.param}]`);
        elements.forEach(element => {
            element.textContent = '';
        });
    }

    /**
     * Применяет конфигурацию к полям модального окна.
     * @private
     */
    private applyConfig() {
        for (const key in this.config) {
            if (this.config.hasOwnProperty(key)) {
                const element = this.modal.querySelector(`[${this.param}="${key}"]`);
                if (element) element.textContent = this.config[key];
            }
        }
    }
}