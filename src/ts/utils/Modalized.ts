import { ModalizedDynamic, ModalizedDynamicOptions } from './ModalizedDynamic';
import { CookieManager, CookieOptions } from './CookieManager';

/**
 * Опции для настройки модального окна.
 */
export interface ModalOptions {
    name: string;
    overlay?: boolean;
    headerVisible?: boolean;
    isDynamicContent?: boolean;
    dynamicContentConfig?: { [key: string]: string };
    isOpen?: boolean;
    autoplay?: boolean;
    autoplayDelay?: number; // Задержка перед автоплеем в миллисекундах
    cookieDuration?: number; // Длительность хранения куки в днях
}

/**
 * Класс для управления модальными окнами.
 */
export class Modal {
    private static instances: Map<string, Modal> = new Map();

    private readonly name: string;
    private readonly headerVisible: boolean;
    private overlayEnabled: boolean;
    private readonly isDynamicContent: boolean;
    private readonly dynamicContentConfig: { [key: string]: string };
    private readonly isOpen: boolean;
    private modalLinks: NodeListOf<HTMLElement>;
    private modalElement: HTMLElement | null = null;
    private overlayElement: HTMLElement | null = null;
    private closeButtonElements: NodeListOf<HTMLElement> | null = null;
    private headerElement: HTMLElement | null = null;
    private readonly autoplay: boolean;
    private readonly autoplayDelay: number;
    private readonly cookieDuration: number;

    private readonly escapeKeyCode: number = 27; // Код клавиши Esc
    private readonly linkAttribute: string = 'data-modal-link';
    private readonly modalAttribute: string = 'data-modal-name';
    private readonly overlaySelector: string = '.js-overlay';
    private readonly closeModalClass: string = 'js-close-modal';
    private readonly activeClass: string = '_active';

    /**
     * Конструктор создает экземпляр класса Modal.
     * @param options Настройки модального окна.
     */
    constructor(options: ModalOptions) {
        this.name = options.name;
        this.headerVisible = options.headerVisible ?? false;
        this.overlayEnabled = options.overlay ?? true;
        this.isDynamicContent = options.isDynamicContent ?? false;
        this.dynamicContentConfig = options.dynamicContentConfig ?? {};
        this.isOpen = options.isOpen ?? false;
        this.autoplay = options.autoplay ?? false;
        this.autoplayDelay = options.autoplayDelay ?? 0;
        this.cookieDuration = options.cookieDuration ?? 0;
        this.modalLinks = document.querySelectorAll(`[${this.linkAttribute}="${this.name}"]`);
        Modal.instances.set(this.name, this);
        this.initializeModals();
        this.initializeUrlListener();
        this.handleAutoplay();
        this.initializeKeyboardListener();
    }

    /**
     * Инициализация модальных окон и их обработчиков.
     */
    private initializeModals(): void {
        this.modalElement = document.querySelector(`[${this.modalAttribute}="${this.name}"]`);
        if (this.overlayEnabled) this.initializeOverlay();
        if (this.headerVisible) this.initializeHeader();
        if (this.isOpen) this.openModalHandler();
        this.modalLinks.forEach(link => {
            link.addEventListener('click', (event) => {
                event.preventDefault(); // Предотвращаем переход по ссылке
                const target = event.currentTarget as HTMLElement;

                // Если модалка уже активна, то закрываем её
                if (this.modalElement?.classList.contains(this.activeClass)) {
                    this.closeModalHandler();
                } else {
                    this.openModalHandler();
                    if (target.hasAttribute('data-modal-title')) {
                        this.changeTitleModal(target.getAttribute('data-modal-title'));
                    }
                    if (target.hasAttribute('data-modal-form-name')) {
                        this.changeFormName(target.getAttribute('data-modal-form-name'));
                    }
                }
            });
        });
    }

    /**
     * Инициализация прослушивателя изменений URL.
     */
    private initializeUrlListener(): void {
        const handleUrlChange = () => {
            const urlParams = new URLSearchParams(window.location.search);
            urlParams.get('modal_name') === this.name ? this.showModal() : null;
        };

        window.addEventListener('popstate', handleUrlChange);
        window.addEventListener('hashchange', handleUrlChange);
        handleUrlChange(); // Проверяем URL при загрузке
    }

    private initializeKeyboardListener(): void {
        window.addEventListener('keydown', this.handleKeyboardEvents.bind(this));
    }


    /**
     * Обработка событий клавиатуры.
     * @param event Событие клавиатуры.
     */
    private handleKeyboardEvents(event: KeyboardEvent): void {
        if (event.keyCode === this.escapeKeyCode) {
            this.closeAllModals();
        }
    }

    /**
     * Закрывает все активные модальные окна.
     */
    private closeAllModals(): void {
        const activeModals = document.querySelectorAll(`.${this.activeClass}[${this.modalAttribute}]`);
        activeModals.forEach(modal => {
            modal.classList.remove(this.activeClass);
        });

        const activeOverlays = document.querySelectorAll(`.${this.activeClass}${this.overlaySelector}`);
        activeOverlays.forEach(overlay => {
            overlay.classList.remove(this.activeClass);
        });

        if(this.headerVisible && this.headerElement) {
            this.headerElement.classList.remove(this.activeClass);
        }
    }

    /**
     * Статический метод для закрытия всех модальных окон.
     */
    public static closeAll(): void {
        Modal.instances.forEach((instance) => {
            instance.closeModalHandler();
        });
    }

    /**
     * Инициализация заголовка модального окна, если он включен.
     */
    private initializeHeader(): void {
        if (this.headerVisible) {
            this.headerElement = document.querySelector('.js-header');
            if (this.isOpen && this.headerElement) {
                this.headerElement.classList.add(this.activeClass);
            }
        }
    }

    /**
     * Обработчик события для открытия модального окна.
     */
    private openModalHandler(): void {
        if (!this.modalElement) return; // Проверка на null
        this.closeAllModals();
        this.showModal();
    }

    /**
     * Обработчик события по смене заголовка в модальном окне.
     */
    private changeTitleModal(text: string | null): void {
        if (!this.modalElement) return;
        const title: HTMLElement | null = this.modalElement.querySelector('[data-modal-title]');
        title && text ? title.textContent = text : null;
    }

    /**
     * Обработчик события по смене названия формы.
     */
    private changeFormName(text: string | null): void {
        if (!this.modalElement) return;
        const input: HTMLInputElement | null = this.modalElement.querySelector('[data-modal-form-name]');
        input && text ? input.value = text : null;
    }

    /**
     * Обработчик события для закрытия модального окна.
     */
    private closeModalHandler(): void {
        this.showModal(false);
        // Устанавливаем куки после закрытия модального окна
        CookieManager.setCookie(this.name, 'true', { 'max-age': this.cookieDuration * 24 * 60 * 60 });
    }

    /**
     * Инициализация оверлея и его обработчиков.
     */
    private initializeOverlay(): void {
        this.overlayElement = document.querySelector(this.overlaySelector);
        if (this.overlayElement) {
            this.overlayElement.addEventListener('click', this.closeModalHandler.bind(this));
        } else {
            this.overlayEnabled = false;
        }
    }

    /**
     * Показ или скрытие модального окна.
     * @param visible Должно ли окно быть видимым.
     */
    private showModal(visible: boolean = true): void {
        if (!this.modalElement) return;
        const isActive = this.modalElement.classList.contains(this.activeClass);
        if (visible === isActive) return;
        visible ? this.activateModal() : this.deactivateModal();
    }

    /**
     * Активация модального окна и связанных элементов.
     */
    private activateModal(): void {
        this.modalElement?.classList.add(this.activeClass);
        this.toggleOverlay(true);
        this.toggleHeader(true);
        this.handleDynamicContent();
        this.emitEvent('modalOpened');

        // Добавление обработчиков событий после активации модального окна
        this.addCloseButtonHandlers();
    }

    /**
     * Деактивация модального окна и связанных элементов.
     */
    private deactivateModal(): void {
        this.modalElement?.classList.remove(this.activeClass);
        this.toggleOverlay(false);
        this.toggleHeader(false);
        this.emitEvent('modalClosed');
    }

    /**
     * Переключение видимости оверлея.
     */
    private toggleOverlay(enable: boolean): void {
        if (this.overlayEnabled) {
            this.overlayElement?.classList.toggle(this.activeClass, enable);
        }
    }

    /**
     * Переключение видимости заголовка.
     */
    private toggleHeader(enable: boolean): void {
        if (this.headerVisible && this.headerElement) {
            this.headerElement.classList.toggle(this.activeClass, enable);
        }
    }

    /**
     * Обработка динамического содержимого модального окна.
     */
    private handleDynamicContent(): void {
        if (this.isDynamicContent && this.dynamicContentConfig && this.modalElement) {
            const dynamicOptions: ModalizedDynamicOptions = {
                modal: this.modalElement,
                config: this.dynamicContentConfig as { [key: string]: string },
            };
            new ModalizedDynamic(dynamicOptions);
        }
    }

    /**
     * Генерация события с наименованием и деталями модального окна.
     * @param eventName Название события.
     */
    private emitEvent(eventName: string): void {
        const event = new CustomEvent(eventName, {
            detail: {
                modalName: this.name,
                modalElement: this.modalElement
            }
        });
        window.dispatchEvent(event);
    }

    /**
     * Добавление обработчиков кнопок закрытия.
     */
    private addCloseButtonHandlers(): void {
        if (this.modalElement) {
            this.closeButtonElements = this.modalElement.querySelectorAll(`.${this.closeModalClass}`);
            this.closeButtonElements.forEach(button => {
                button.addEventListener('click', this.closeModalHandler.bind(this));
            });
        }
    }

    /**
     * Обработка автоматического показа модального окна.
     */
    private handleAutoplay(): void {
        if (this.autoplay && !CookieManager.getCookie(this.name)) {
            setTimeout(() => {
                this.openModalHandler();
            }, this.autoplayDelay);
        }
    }

    /**
     * Статический метод для открытия модального окна.
     */
    public static open(name: string, data?: { [key: string]: string }): void {
        const instance = Modal.instances.get(name);
        if (instance) {
            instance.openModalHandler();
            if (data?.formName) {
                instance.changeFormName(data.formName);
            }
        }
    }

    /**
     * Статический метод для закрытия модального окна.
     */
    public static close(name: string): void {
        const instance = Modal.instances.get(name);
        if (instance) {
            instance.closeModalHandler();
        }
    }

    static toggle(modalName: string): void {
        const modal = Modal.instances.get(modalName);
        if (modal && modal.modalElement) {
            const shouldBeVisible = !modal.modalElement.classList.contains(modal.activeClass);
            modal.showModal(shouldBeVisible);
        } else {
            console.error('Modal instance or modal element not found for:', modalName);
        }
    }
}
