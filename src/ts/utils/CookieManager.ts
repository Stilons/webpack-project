export interface CookieOptions {
    path?: string;
    expires?: Date | string | number;
    domain?: string;
    secure?: boolean;
    'max-age'?: number;

    [key: string]: any;
}

export class CookieManager {
    public static getCookie(name: string): string | undefined {
        const matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    public static setCookie(name: string, value: string, options: CookieOptions = {}): void {
        let updatedCookie = `${encodeURIComponent(name)}=${encodeURIComponent(value)}`;

        if (options.expires) {
            if (typeof options.expires === 'number') {
                const date = new Date();
                date.setTime(date.getTime() + options.expires * 1000);
                options.expires = date.toUTCString();
            } else if (options.expires instanceof Date) {
                options.expires = options.expires.toUTCString();
            }
        }

        for (const [key, optionValue] of Object.entries(options)) {
            updatedCookie += `; ${key}`;
            if (optionValue !== true) {
                updatedCookie += `=${optionValue}`;
            }
        }

        document.cookie = updatedCookie;
    }

    public static deleteCookie(name: string): void {
        this.setCookie(name, "", {'max-age': -1});
    }
}

// Пример использования
const cookieName = "example";
const cookieValue = "test";
const cookieOptions: CookieOptions = {
    expires: 3600, // 1 час
    path: '/',
    secure: true
};

// Установить cookie
CookieManager.setCookie(cookieName, cookieValue, cookieOptions);

// Получить значение cookie
const retrievedValue = CookieManager.getCookie(cookieName);

// Удалить cookie
CookieManager.deleteCookie(cookieName);
