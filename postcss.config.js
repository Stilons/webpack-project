module.exports = {
    plugins: [
        require('autoprefixer'),
        require('postcss-import'),
        require('postcss-custom-media'),
        require('cssnano')({
            preset: 'default',
        })
    ]
};
